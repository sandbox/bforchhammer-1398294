<?php
/**
 * ParentFinder plugin for YABAPI module.
 *
 * Checks whether the context path has a menu item, and returns the path of the respective parent menu item.
 */
 
$plugin = array(
  'title' => t('Menu'),
  'handler' => 'yabapi_menu_parentfinder',
);
 
class yabapi_menu_parentfinder implements yabapi_parentfinder_interface {
  /**
   * Check menu for context path; if a menu item exists, return path of parent item.
   */
  function findParent(yabapi_context $context) {
    $menu_link = yabapi_menu_link_by_path($context->path);
    if (!empty($menu_link['plid'])) {
      $parent_menu_link = yabapi_menu_link_by_mlid($menu_link['plid']);
      return $parent_menu_link['link_path'];
    }
    return FALSE;
  }
}