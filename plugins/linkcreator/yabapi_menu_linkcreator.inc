<?php
/**
 * LinkCreator plugin for YABAPI module.
 *
 * Tries to build link title from a respective menu router. 
 */
 
$plugin = array(
  'title' => t('Menu Title'),
  'handler' => 'yabapi_menu_linkcreator',
);
 
class yabapi_menu_linkcreator implements yabapi_linkcreator_interface {

  function getLink(yabapi_context $context) {
    if (!empty($context->router['title'])) {
      return l($context->router['title'], $context->getPath(), $context->router['localized_options']);
    }
  }

}