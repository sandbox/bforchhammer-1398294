<?php

/**
 *
 */
interface yabapi_parentfinder_interface {

  /**
   * Determine a parent path based on the given context.
   * 
   * @param Page context which to find a parent path for.
   * @return String if parent path has been found, FALSE otherwise.
   */
  function findParent(yabapi_context $context);
}
