<?php

/**
 * YABAPI Context class.
 * 
 * Takes a path and tries to load related entities (e.g. the user or node).
 */
class yabapi_context {
  
  var $path;
  var $router;
  var $access;

  var $entity_type = NULL;
  var $object = NULL;

  function __construct($path) {
    $this->path = $path;

    $this->router = menu_get_item($this->path);
    $this->access = $this->router['access'];

    // Assume that entities use a load path of format "entity_type/%id"
    if (isset($this->router['map'][1]) && is_object($this->router['map'][1])) {
      $this->object = $this->router['map'][1];
      $this->entity_type = $this->router['map'][0];
    }  }

  function getPath() {
    return $this->path;
  }

  function getEntityType() {
    return $this->entity_type;
  }

  function isNode() {
    return $this->entity_type == 'node';
  }
  
  function isUser() {
    return $this->entity_type == 'user';
  }

  function getNode() {
    return $this->isNode() ? $this->object : NULL;
  }

  function getUser() {
    return $this->isUser() ? $this->object : NULL;
  }
}
