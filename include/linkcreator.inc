<?php

/**
 * @file 
 * Functions related to creating the breadcrumb links, i.e. transforming the trail into renderable items.
 */

/**
 * Build the breadcrumb links from the trail of context objects.
 */
function yabapi_build_breadcrumb_links(array $trail) {
  $links = array();  

  foreach ($trail as $context) {
    $link = _yabapi_create_link($context);
    if (!empty($link)) {
      $links[] = $link;
    }
    else {
      $links[] = l(t('Title not found'), $context->getPath());
    }
  }
    
  return $links;
}

/**
 * Tries to determine a title by evaluating all enabled 'linkcreator' plugins.
 */
function _yabapi_create_link(yabapi_context $context) {
  $linkcreators = &drupal_static(__FUNCTION__);

  if (!$linkcreators) {
    // build list of valid linkcreator class names
    $linkcreators = array();

    $plugins = yabapi_get_enabled_linkcreator_plugins();
    foreach ($plugins as $plugin) {
      $class = ctools_plugin_get_class($plugin, 'handler');
      
      $check = new ReflectionClass($class);
      if ($check->implementsInterface('yabapi_linkcreator_interface')) {
        $linkcreators[] = $class;
      }
    }
  }

  foreach ($linkcreators as $classname) {
    $instance = _yabapi_linkcreator_instance($classname);
    $link = $instance->getLink($context);
    
    if ($link) return $link;
  }
  return NULL;
}

/**
 * Returns an instance of the given parentfinder classname.
 */
function _yabapi_linkcreator_instance($id) {
  $instances = &drupal_static(__FUNCTION__);
 
  if (!isset($instances[$id])) {
    $instances[$id] = new $id();
  }
 
  return $instances[$id];
}

/**
 * Returns a list of enabled and sorted plugins, as defined on the admin page.
 */
function yabapi_get_enabled_linkcreator_plugins() { 
  // get all plugins (info arrays)
  ctools_include('plugins');
  $plugins = ctools_get_plugins('yabapi', 'linkcreator');

  $plugins = empty($plugins) ? array() : $plugins;
  uasort($plugins, 'ctools_plugin_sort');

  // @todo provide admin interface for enabling/disabling + sorting

  return $plugins;
}
