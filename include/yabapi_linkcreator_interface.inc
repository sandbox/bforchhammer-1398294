<?php

/**
 *
 */
interface yabapi_linkcreator_interface {

  /**
   * Create a fully rendered link for the given path context.
   *
   * @param $context
   *   Page context which to create a title for.
   * @return
   *   Fully rendered link if title can be found/generated, FALSE otherwise.
   */
  function getLink(yabapi_context $context);
}
