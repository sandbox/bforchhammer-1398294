<?php

/**
 * @file 
 * Functions related to building the breadcrumb trail.
 */

/**
 * Build the trail of parent paths.
 *
 * Max recursion depth is 7.
 */
function yabapi_build_breadcrumb_trail(yabapi_context $context) {
  
  $trail = array();
  $depth = 0;
  $max_depth = variable_get('yabapi_recursion_max_depth', 7);
  
  // try to find a parent context/path until we reach the top
  do {
    if ($context->access) $trail[] = $context;
    $context = _yabapi_find_parent($context);
  }
  while ($context != NULL && ++$depth < $max_depth);
  
  // reverse to "parent > child" order
  $trail = array_reverse($trail);
  
  return $trail;
}

/**
 * Tries to determine a parent path by evaluating all enabled 'parentfinder' plugins.
 */
function _yabapi_find_parent(yabapi_context $context) {
  $parentfinders = &drupal_static(__FUNCTION__);
  
  if (!$parentfinders) {
    // build list of valid parentfinder class names
    $parentfinders = array();

    $plugins = yabapi_get_enabled_parentfinder_plugins();
    foreach ($plugins as $plugin) {
      $class = ctools_plugin_get_class($plugin, 'handler');
      
      $check = new ReflectionClass($class);
      if ($check->implementsInterface('yabapi_parentfinder_interface')) {
        $parentfinders[] = $class;
      }
      else {
        // error message?
      }
    }
  }

  // for each plugin try to find a parent candidate.
  foreach ($parentfinders as $classname) {
    $instance = _yabapi_parentfinder_instance($classname);
    $candidate = $instance->findParent($context);
    if ($candidate != NULL) {
      return ($candidate instanceof yabapi_context) ? $candidate : new yabapi_context($candidate);
    }
  }
  return NULL;
}

/**
 * Returns an instance of the given parentfinder classname.
 */
function _yabapi_parentfinder_instance($id) {
  $instances = &drupal_static(__FUNCTION__);
 
  if (!isset($instances[$id])) {
    $instances[$id] = new $id();
  }
 
  return $instances[$id];
}

/**
 * Returns a list of enabled and sorted plugins, as defined on the admin page.
 */
function yabapi_get_enabled_parentfinder_plugins() { 
  // get all plugins (info arrays)
  ctools_include('plugins');
  $plugins = ctools_get_plugins('yabapi', 'parentfinder');

  $plugins = empty($plugins) ? array() : $plugins;
  uasort($plugins, 'ctools_plugin_sort');

  // @todo provide admin interface for enabling/disabling + sorting

  return $plugins;
}
