<?php

/**
 * @file
 * API documentation file
 */

/**
 * Alter the generated breadcrumb trail.
 *
 * @param $trail
 *   Array of yabapi_context objects. 
 */
function hook_yabapi_breadcrumb_trail_alter(array &$trail) {
  // Remove items of type "ignore_me", unless it has a specific menu item.
  foreach ($trail as $i => $item) {
    if ($item->isNode() && $item->getNode()->type == 'ignore_me') {
      $menu_item = yabapi_menu_link_by_path($item->getPath());
      if ($menu_item['link_path'] == 'node/%') unset($trail[$i]);
    }
  }
}

/**
 * Alter the final array of breadcrumb links.
 *
 * @param $links
 *   Array of links (<a href...). 
 */
function hook_yabapi_breadcrumb_links_alter(array &$links) {
  // Add a Frontpage link to all pages but the front page itself
  if (count($links) && !drupal_is_front_page()) {
    array_unshift($links, l(t('Frontpage'), '<front>'));
  }
  // Remove the node title on the frontpage
  if (count($links) == 1) {
    array_pop($links);
  }
}

/**
 * Implement this hook to tell ctools where to find your parentfinder and
 * linkcreator plugins. 
 * 
 * Every .inc file in the respective directories represents one plugin. Every
 * plugin file needs to contain a global array variable which specifies the
 * plugin class and the name of the plugin. The plugin class needs to be
 * implement the respective plugin interface, e.g.
 * <code>yabapi_parentfinder_interface</code>.
 *
 * Please have a look at the plugins directory for an example of what a plugin
 * looks like. 
 * 
 * @param $module
 *   The module requesting a plugin, yabapi in this case.
 * @param $plugin
 *   The name of the plugin; in this case one of: parentfinder, linkcreator
 * 
 * @return
 *   A path relative to the module directory (string).
 */
function hook_ctools_plugin_directory($module, $plugin) {
  if ($module == 'yabapi' && $plugin == 'parentfinder') {
    return 'plugins/parentfinder';
  }
  if ($module == 'yabapi' && $plugin == 'linkcreator') {
    return 'plugins/linkcreator';
  }
}